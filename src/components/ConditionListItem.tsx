import { Button } from 'primereact/button';
import { Tooltip } from 'primereact/tooltip';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { operatorIcon } from '../helper/OperatorIconSwitcher';
import { ConditionDatabaseProps } from '../models/model';

interface ConditionListItemProps {
  conditionGroup: ConditionDatabaseProps;
}

const ConditionListItem: FC<ConditionListItemProps> = ({ conditionGroup }) => {
  const navigate = useNavigate();
  const timestamp = new Date(conditionGroup.data.timestamp.seconds * 1000);
  return (
    <li>
      <div className='bg-cyan-100 p-4 my-4 rounded-xl shadow-md'>
        <div className='flex w-full'>
          <div className='w-1/2'>
            <p>
              Generating ID: <span className='font-semibold'>{conditionGroup.id}</span>
            </p>
            <p>
              Generating time:
              <span className='font-semibold'>{` ${timestamp.toLocaleDateString()} ${timestamp.toLocaleTimeString()}`}</span>
            </p>
            <p className='mb-4'>Conditions:</p>
          </div>
          <div className='w-1/2 text-end'>
            <Button
              className='font-semibold w-auto border-2 rounded-lg p-2 bg-cyan-500 hover:bg-cyan-700 text-white'
              onClick={() => navigate(`/execution/${conditionGroup.id}`)}
            >
              Execute Script
            </Button>
          </div>
        </div>
        <div>
          {conditionGroup.data.items.map((item, index) => (
            <div key={index}>
              <div className='flex gap-4 items-center justify-around mt-2'>
                <p className='text-lg text-black font-semibold text-center w-[40%]'>{item.propertyName}</p>
                <Tooltip target='.operator-button' className='text-sm shadow-none' />
                <Button
                  className='bg-white p-4 operator-button'
                  rounded
                  text
                  raised
                  aria-label='Filter'
                  data-pr-tooltip={item.id.toString()}
                  data-pr-position='top'
                >
                  {operatorIcon(item.operator)}
                </Button>
                <p className='text-lg text-black font-semibold text-center w-[40%]'>{item.value}</p>
              </div>
              {index < conditionGroup.data.items.length - 1 && <hr className='bg-gray-400 border-0 h-px my-4' />}
            </div>
          ))}
        </div>
      </div>
    </li>
  );
};

export default ConditionListItem;
