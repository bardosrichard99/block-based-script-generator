import { FaUser, FaUserCheck } from 'react-icons/fa';
import { TiHome } from 'react-icons/ti';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

const NavBar = () => {
  const navigate = useNavigate();
  const { user } = useAuth();

  const loginDirecter = () => {
    if (user.uid) {
      navigate('/profile');
    } else {
      navigate('/signin');
    }
  };

  return (
    <nav className='w-full flex bg-zinc-200 h-[70px]'>
      <div className='px-4 md:px-16 py-6 items-center transition duration-500 w-[20%]'>
        <TiHome size={26} className='  cursor-pointer object-cover' onClick={() => navigate('/')} />
      </div>
      <div className='px-4 md:px-16 py-6 text-center transition duration-500 w-[60%]'>
        <h3>Block-based Script Generator</h3>
      </div>
      <div className='px-4 md:px-16 flex justify-end cursor-pointer w-[20%] my-auto'>
        {user?.uid ? (
          <FaUserCheck size={26} className='text-black transition block' onClick={loginDirecter} />
        ) : (
          <FaUser size={26} className='text-black transition block' onClick={loginDirecter} />
        )}
      </div>
    </nav>
  );
};

export default NavBar;
