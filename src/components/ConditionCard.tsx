import { Button } from 'primereact/button';
import React from 'react';
import { FaTrash } from 'react-icons/fa';
import { toast } from 'react-toastify';
import { useConditionList } from '../context/ConditionListContext';
import { operatorIcon } from '../helper/OperatorIconSwitcher';
import { Condition } from '../models/model';

interface ConditionCardProps {
  data: Condition;
  isDeleteAllowed: boolean;
}

const ConditionCard: React.FC<ConditionCardProps> = ({ data, isDeleteAllowed }) => {
  const { removeFromConditionList } = useConditionList();

  const onConditionDelete = (id: number) => {
    removeFromConditionList(id);
    toast.success('Condition deleted successfully');
  };

  return (
    <div className='bg-cyan-100 p-4 my-4 rounded-xl shadow-md'>
      <div className='flex w-full justify-between'>
        <p className='text-left'>
          Condition ID: <span className='font-semibold'>{data.id}</span>
        </p>
        {isDeleteAllowed && (
          <Button className='w-fit' onClick={() => onConditionDelete(data.id)}>
            <FaTrash className='text-red-500' />
          </Button>
        )}
      </div>
      <div className='flex gap-4 items-center justify-around mt-2'>
        <p className='text-lg text-black font-semibold text-center w-[40%]'>{data.propertyName}</p>
        <Button className='bg-white p-4' rounded text raised aria-label='Filter'>
          {operatorIcon(data.operator)}
        </Button>
        <p className='text-lg text-black font-semibold text-center w-[40%]'>{data.value}</p>
      </div>
    </div>
  );
};

export default ConditionCard;
