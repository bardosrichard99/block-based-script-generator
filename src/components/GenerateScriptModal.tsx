import { Button } from 'primereact/button';
import { SelectButton, SelectButtonChangeEvent } from 'primereact/selectbutton';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

import { doc, serverTimestamp, setDoc } from 'firebase/firestore';
import { Tooltip } from 'primereact/tooltip';
import { FaRegCopy } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { useConditionList } from '../context/ConditionListContext';
import { db } from '../firebase';
import { Condition } from '../models/model';

const operatorSwitch = (operator: string, isJS: boolean) => {
  switch (operator) {
    case 'Equal':
      return isJS ? '===' : '=';
    case 'Not Equal':
      return isJS ? '!==' : '<>';
    case 'Greater than':
      return '>';
    case 'Less than':
      return '<';
    case 'Greater than or equal':
      return '>=';
    case 'Less than or equal':
      return '<=';
  }
};

const GenerateScriptModal = () => {
  const { user } = useAuth();
  const navigate = useNavigate();
  const { conditionListItems, emptyConditionList } = useConditionList();
  const [selectedScriptType, setSelectedScriptType] = useState<string>('JavaScript');
  const [generatedScript, setGeneratedScript] = useState<string>('');
  const scriptTypes: string[] = ['JavaScript', 'SQL'];

  useEffect(() => {
    let script = '';
    if (selectedScriptType === 'JavaScript') {
      conditionListItems.forEach((item: Condition, index: number) => {
        if (index !== 0) script += ' else ';
        script += `if(${item.propertyName} ${operatorSwitch(item.operator, true)} "${item.value}"){\n\t// code\n}`;
        if (index + 1 === conditionListItems.length) script += ';';
      });
    } else if (selectedScriptType === 'SQL') {
      script = 'SELECT\n*\nFROM\ntable_name\nWHERE\n';
      conditionListItems.forEach((item: Condition, index: number) => {
        if (index !== 0) script += '\nAND\n';
        script += `${item.propertyName} ${operatorSwitch(item.operator, false)} "${item.value}"`;
      });
    }
    setGeneratedScript(script);
  }, [selectedScriptType, conditionListItems]);

  const onScriptDownload = () => {
    const a = window.document.createElement('a');
    a.href = window.URL.createObjectURL(new Blob([generatedScript], { type: 'text/plain' }));
    a.download = 'script.txt';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };

  const onScriptSave = async () => {
    if (!user.uid) {
      toast.error('Please login first');
      return;
    } else if (!conditionListItems.length) {
      toast.error('Please add items to the cart');
      return;
    } else {
      const conditionData = {
        userId: user.uid,
        items: conditionListItems,
        timestamp: serverTimestamp(),
      };
      const date = new Date();
      await setDoc(doc(db, 'conditions', date.getTime().toString()), conditionData);
      emptyConditionList();
      toast.success('Condition list successfully saved');
      navigate('/profile');
    }
  };

  const onCopyToClipboard = async () => {
    await navigator.clipboard.writeText(generatedScript);
    toast.success('Copied to clipboard');
  };

  return (
    <div className='w-full rounded-lg'>
      <div className='text-center'>
        <SelectButton
          className='w-full my-2'
          value={selectedScriptType}
          onChange={(e: SelectButtonChangeEvent) => (e.value ? setSelectedScriptType(e.value) : null)}
          options={scriptTypes}
          pt={{ button: { className: 'border-gray-300 border-2 w-1/4' } }}
        />
      </div>
      <div className='bg-gray-100 w-full h-[200px] rounded-lg mb-4 p-2 relative'>
        <Tooltip target='.copy-script' className='text-sm shadow-none' />
        <FaRegCopy
          className='copy-script absolute right-2 cursor-pointer hover:shadow-lg'
          onClick={onCopyToClipboard}
          data-pr-tooltip='Copy to clipboard'
          data-pr-position='bottom'
        />
        <pre className='overflow-scroll h-full'>{generatedScript}</pre>
      </div>
      <div className='flex justify-around'>
        <Button
          className='font-semibold w-1/2 border-2 rounded-lg p-2 bg-cyan-500 hover:bg-cyan-700 text-white justify-center'
          onClick={onScriptSave}
        >
          Save to profile
        </Button>
        <Button
          className='font-semibold w-1/2 border-2 rounded-lg p-2 bg-cyan-500 hover:bg-cyan-700 text-white justify-center'
          onClick={onScriptDownload}
        >
          Download
        </Button>
      </div>
    </div>
  );
};

export default GenerateScriptModal;
