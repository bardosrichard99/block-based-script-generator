import { useState } from 'react';
import { FaLock, FaUser } from 'react-icons/fa';
import { IoIosArrowForward, IoMdEye, IoMdEyeOff } from 'react-icons/io';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import NavBar from './components/NavBar';
import OAuth from './components/OAuth';
import { useAuth } from './context/AuthContext';
import { SignInFormDataProperties } from './models/model';

const SignIn = () => {
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [formData, setFormData] = useState<SignInFormDataProperties>({
    email: '',
    password: '',
  });
  const { ...allData } = formData;
  const { logIn } = useAuth();
  const navigate = useNavigate();
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { id, value } = e.target;
    setFormData(prevState => ({
      ...prevState,
      [id]: value,
    }));
  };
  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      await logIn(allData.email, allData.password);
      toast.success('Successfully logged in');
      navigate('/');
    } catch (error) {
      toast.error('Bad User Credentials');
    }
  };
  const canSubmit = [...Object.values(allData)].every(Boolean);
  return (
    <>
      <NavBar />
      <div className='m-4'>
        <header>
          <p className='text-3xl font-extrabold mt-8 mb-4'>Welcome back</p>
        </header>
        <main>
          <form onSubmit={onSubmit}>
            <div className='relative'>
              <input
              data-testid='email-input'
                type='email'
                className='shadow-[rgba(0,0,0,0.11)] h-12 w-full text-base px-12 py-0 rounded-[3rem] border-0 bg-white my-4'
                placeholder='Email'
                id='email'
                value={allData.email}
                onChange={onChange}
              />
              <div className='absolute inset-y-0 left-0 pl-5 flex items-center pointer-events-none'>
                <FaUser />
              </div>
            </div>
            <div className='relative'>
              <input
              data-testid='password-input'
                type={showPassword ? 'text' : 'password'}
                className='shadow-[rgba(0,0,0,0.11)] h-12 w-full text-base px-12 py-0 rounded-[3rem] border-0 bg-white my-4'
                placeholder='Password'
                id='password'
                value={allData.password}
                onChange={onChange}
              />
              <div className='absolute inset-y-0 left-0 pl-5 flex items-center pointer-events-none'>
                <FaLock />
              </div>
              <div className='absolute inset-y-0 right-0 pr-5 flex items-center cursor-pointer'>
                {showPassword ? (
                  <IoMdEyeOff size={24} onClick={() => setShowPassword(prevState => !prevState)} />
                ) : (
                  <IoMdEye size={24} onClick={() => setShowPassword(prevState => !prevState)} />
                )}
              </div>
            </div>
            <a href='/forgot-password' className='cursor-pointer text-cyan-500 font-semibold text-right'>
              Forgot Password
            </a>
            <div className='flex justify-center mt-12'>
              <button
              data-testid='submit-button'
                disabled={!canSubmit}
                className={`cursor-pointer flex items-center ${!canSubmit && 'opacity-50 cursor-not-allowed'}`}
              >
                <p className=' text-2xl font-bold'>Sign In</p>
                <div className=' flex justify-center items-center w-10 h-10 bg-cyan-500 rounded-[50%] ml-4'>
                  <IoIosArrowForward fill='#ffffff' size={24} />
                </div>
              </button>
            </div>
          </form>
          <OAuth />
          <div className='flex justify-center items-center mt-6'>
            <a href='/signup' className='text-cyan-500 font-semibold text-center mt-16 mb-12'>
              Sign Up Instead
            </a>
          </div>
        </main>
      </div>
    </>
  );
};
export default SignIn;
