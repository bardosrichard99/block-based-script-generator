import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Dropdown, DropdownChangeEvent } from 'primereact/dropdown';
import { FileUpload, FileUploadSelectEvent } from 'primereact/fileupload';
import { InputText } from 'primereact/inputtext';
import { SelectButton, SelectButtonChangeEvent } from 'primereact/selectbutton';
import { Tooltip } from 'primereact/tooltip';
import React, { useEffect, useState } from 'react';
import { FaTrash } from 'react-icons/fa';
import { toast } from 'react-toastify';
import ConditionCard from './components/ConditionCard';
import GenerateScriptModal from './components/GenerateScriptModal';
import NavBar from './components/NavBar';
import { useConditionList } from './context/ConditionListContext';
import { Condition, UploadFileType, UploadedFileObj } from './models/model';

const App = () => {
  const { conditionListItems, addToConditionList, emptyConditionList } = useConditionList();
  const [parameterValue, setParameterValue] = useState<string | null>('');
  const [conditionValue, setConditionValue] = useState<string>('');
  const [selectedOperatorType, setSelectedOperatorType] = useState<string>('String');
  const [operators, setOperators] = useState<string[]>(['Equal', 'Not Equal']);
  const [selectedOperator, setSelectedOperator] = useState<string | null>(null);
  const [uploadedJson, setUploadedJson] = useState<UploadedFileObj | null>(null);

  const [dialogVisible, setDialogVisible] = useState<boolean>(false);

  const operatorTypes: string[] = ['String', 'Number', 'Boolean'];

  useEffect(() => {
    if (selectedOperatorType === 'Number') {
      setOperators(['Equal', 'Not Equal', 'Greater than', 'Less than', 'Greater than or equal', 'Less than or equal']);
    } else {
      setOperators(['Equal', 'Not Equal']);
    }
  }, [selectedOperatorType]);

  const conditionSubmission = () => {
    if (parameterValue === '' || parameterValue === null) {
      toast.error('Please enter a parameter value');
    } else if (selectedOperator === null) {
      toast.error('Please choose an operator');
    } else if (selectedOperatorType === 'Boolean' && conditionValue === '') {
      toast.error('Please choose a Boolean value');
    } else {
      const conditionItem: Condition = {
        id: Math.floor(new Date().getTime() / 1000),
        propertyName: parameterValue,
        operator: selectedOperator,
        value: conditionValue,
      };
      addToConditionList(conditionItem);
      toast.success('Condition added successfully');
      setParameterValue('');
      setConditionValue('');
      setSelectedOperator(null);
    }
  };

  const onJsonUpload = async (e: FileUploadSelectEvent) => {
    try {
      const file = e.files[0] as UploadFileType;
      const response = await fetch(file.objectURL);
      const data = await response.json();
      setUploadedJson(data);
      setParameterValue(null);
    } catch (error) {
      toast.error('Uploading file failed');
    }
  };

  return (
    <>
      <Dialog
        header='Generated Script'
        visible={dialogVisible}
        style={{ width: '60vw' }}
        onHide={() => setDialogVisible(false)}
      >
        <GenerateScriptModal />
      </Dialog>
      <NavBar />
      <main className='pb-4'>
        <div className='h-full flex flex-wrap p-4 gap-1 justify-between'>
          <div className='h-fit w-[29%]'>
            <div className='rounded-xl shadow-md bg-white p-4 mt-4 mb-6'>
              <h2 className='font-semibold rounded-lg mb-2'>Add Condition</h2>
              <div className='text-center'>
                {!uploadedJson ? (
                  <InputText
                    className='border-2 rounded-lg w-full p-2 my-2'
                    placeholder='Object parameter'
                    value={parameterValue ?? ''}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setParameterValue(e.target.value)}
                  />
                ) : (
                  <Dropdown
                    className='border-2 rounded-lg w-full my-2'
                    value={parameterValue}
                    onChange={(e: DropdownChangeEvent) => setParameterValue(e.value)}
                    options={Object.keys(uploadedJson)}
                    showClear
                    placeholder='Select a parameter'
                  />
                )}
                <SelectButton
                  className='w-full my-2'
                  value={selectedOperatorType}
                  onChange={(e: SelectButtonChangeEvent) => (e.value ? setSelectedOperatorType(e.value) : null)}
                  options={operatorTypes}
                  pt={{ button: { className: 'border-gray-300	border-2' } }}
                />
                <Dropdown
                  className='border-2 rounded-lg w-full my-2'
                  value={selectedOperator}
                  onChange={(e: DropdownChangeEvent) => setSelectedOperator(e.value)}
                  options={operators}
                  showClear
                  placeholder='Select an operator'
                />
                {selectedOperatorType !== 'Boolean' ? (
                  <InputText
                    className='border-2 rounded-lg w-full p-2 my-2'
                    placeholder='Value'
                    value={conditionValue}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setConditionValue(e.target.value)}
                  />
                ) : (
                  <Dropdown
                    className='border-2 rounded-lg w-full my-2'
                    value={conditionValue}
                    onChange={(e: DropdownChangeEvent) => setConditionValue(e.value)}
                    options={['true', 'false']}
                    showClear
                    placeholder='Select a parameter'
                  />
                )}
              </div>
              <Button
                className='border-2 rounded-lg w-full p-2 my-2 bg-cyan-500 hover:bg-cyan-700 text-white'
                label='Add'
                onClick={conditionSubmission}
              />
            </div>
            {!uploadedJson ? (
              <>
                <Tooltip target='.file-upload-button' className='text-sm shadow-none'>
                  <pre>{'Format:\n{\n\t"key1": "value",\n\t"key2": "value"\n}'}</pre>
                </Tooltip>
                <FileUpload
                  className='w-full file-upload-button'
                  mode='basic'
                  data-pr-position='bottom'
                  name='demo[]'
                  url='/upload'
                  accept='.json'
                  maxFileSize={1000000}
                  onSelect={e => onJsonUpload(e)}
                  chooseLabel='Upload File (JSON)'
                />
              </>
            ) : (
              <Button
                className='border-2 rounded-lg w-full p-2 my-2 bg-red-500 hover:bg-red-700 text-white'
                label='JSON Uploaded'
                onClick={() => setUploadedJson(null)}
              >
                <FaTrash className='text-white' />
              </Button>
            )}
          </div>

          <div className='h-full w-[69%] bg-white p-4 mb-4 my-4 rounded-xl shadow-md overflow-hidden'>
            <div className='flex w-full'>
              <div className='w-1/2'>
                <h2 className='font-semibold w-full'>Conditions</h2>
                {conditionListItems.length ? (
                  <h4 className='w-full text-red-500 cursor-pointer text-sm' onClick={emptyConditionList}>
                    Delete all
                  </h4>
                ) : null}
              </div>
              <div className='w-1/2 text-end'>
                {conditionListItems.length ? (
                  <Button
                    className='font-semibold w-auto border-2 rounded-lg p-2 bg-cyan-500 hover:bg-cyan-700 text-white'
                    onClick={() => setDialogVisible(true)}
                  >
                    Generate Script
                  </Button>
                ) : null}
              </div>
            </div>
            <div className='overflow-y-auto h-full'>
              {conditionListItems.map((condition: Condition) => (
                <ConditionCard key={condition.id} data={condition} isDeleteAllowed={true} />
              ))}
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default App;
