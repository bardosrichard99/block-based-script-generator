import React, { createContext, useContext, useEffect, useState } from 'react';
import { Condition, ConditionListContextType } from '../models/model';

const ConditionListContext = createContext<ConditionListContextType>({
  conditionListItems: [],
  addToConditionList: () => {},
  removeFromConditionList: () => {},
  emptyConditionList: () => {},
});

export const useConditionList = () => useContext<any>(ConditionListContext);

function getInitialState() {
  const notes = localStorage.getItem('conditions');
  return notes ? JSON.parse(notes) : [];
}

export const ConditionListContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [conditionListItems, setConditionListItems] = useState<Condition[]>(getInitialState);

  useEffect(() => {
    localStorage.setItem('conditions', JSON.stringify(conditionListItems));
  }, [conditionListItems]);

  // Add to Condition List
  const addToConditionList = (item: Condition) => {
    setConditionListItems(prevItems => [item, ...prevItems]);
  };

  // Remove from Condition List
  const removeFromConditionList = (itemId: number) => {
    setConditionListItems(prevItems => prevItems.filter(item => item.id !== itemId));
  };

  // Empty Condition List
  const emptyConditionList = () => {
    setConditionListItems([]);
  };

  return (
    <ConditionListContext.Provider
      value={{ conditionListItems, addToConditionList, removeFromConditionList, emptyConditionList }}
    >
      {children}
    </ConditionListContext.Provider>
  );
};
