import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
  updateProfile,
} from 'firebase/auth';
import { doc, serverTimestamp, setDoc } from 'firebase/firestore';
import React, { createContext, useContext, useEffect, useState } from 'react';
import { auth, db } from '../firebase';
import { FormDataCopyProperties, SignUpFormDataProperties, UserType } from '../models/model';

const AuthContext = createContext({});

export const useAuth = () => useContext<any>(AuthContext);

export const AuthContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [user, setUser] = useState<UserType>({ email: null, uid: null, name: null });
  const [loading, setLoading] = useState<Boolean>(true);

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, user => {
      if (user) {
        setUser({
          email: user.email,
          uid: user.uid,
          name: user.displayName,
        });
      } else {
        setUser({ email: null, uid: null, name: null });
      }
    });

    setLoading(false);

    return () => unsubscribe();
  }, []);

  // Sign up the user
  const signUp = async (formData: SignUpFormDataProperties) => {
    const { ...allData } = formData;
    const userCredential = await createUserWithEmailAndPassword(auth, allData.email, allData.password);

    const user = userCredential.user;

    updateProfile(auth.currentUser!, {
      displayName: allData.name,
    });
    const formDataCopy: FormDataCopyProperties = { ...formData, timestamp: serverTimestamp() };
    delete formDataCopy['password'];

    await setDoc(doc(db, 'users', user.uid), formDataCopy);
  };

  // Login the user
  const logIn = async (email: string, password: string) => {
    try {
      await signInWithEmailAndPassword(auth, email, password);
    } catch (error: any) {
      throw new Error(error.message);
    }
  };

  // Logout the user
  const logOut = async () => {
    setUser({ email: null, uid: null, name: null });
    return await signOut(auth);
  };

  return (
    <AuthContext.Provider value={{ user, signUp, logIn, logOut }}>{loading ? null : children}</AuthContext.Provider>
  );
};
