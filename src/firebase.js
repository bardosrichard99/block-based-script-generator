import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
  apiKey: "AIzaSyCx2GQhO7ZQdAQwQ8s5RBbDwwIwQysp10s",
  authDomain: "blockbased-script-generator.firebaseapp.com",
  projectId: "blockbased-script-generator",
  storageBucket: "blockbased-script-generator.appspot.com",
  messagingSenderId: "293337880410",
  appId: "1:293337880410:web:ac7c031f3c14f4fe91c563"
};

const firebaseApp = initializeApp(firebaseConfig);
export const db = getFirestore(firebaseApp);
export const auth = getAuth();
