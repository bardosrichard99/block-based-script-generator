import { collection, getDocs, query, where } from 'firebase/firestore';
import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { FileUpload, FileUploadSelectEvent } from 'primereact/fileupload';
import { Tooltip } from 'primereact/tooltip';
import { useEffect, useState } from 'react';
import { FaTrash } from 'react-icons/fa';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import ConditionCard from './components/ConditionCard';
import NavBar from './components/NavBar';
import { useAuth } from './context/AuthContext';
import { db } from './firebase';
import { Condition, ConditionDatabaseProps, UploadFileType, UploadedFileObj } from './models/model';

const filterConditionSwitch = (element: UploadedFileObj, item: Condition) => {
  switch (item.operator) {
    case 'Equal':
      return element[item.propertyName] === item.value;
    case 'Not Equal':
      return element[item.propertyName] !== item.value;
    case 'Greater than':
      return element[item.propertyName] > item.value;
    case 'Less than':
      return element[item.propertyName] < item.value;
    case 'Greater than or equal':
      return element[item.propertyName] >= item.value;
    case 'Less than or equal':
      return element[item.propertyName] <= item.value;
  }
};

const ScriptExecution = () => {
  const { conditionGroupId } = useParams();
  const { user } = useAuth();
  const [conditionGroup, setConditionGroup] = useState<ConditionDatabaseProps>();
  const [uploadedJson, setUploadedJson] = useState<UploadedFileObj[] | null>(null);
  const [filteredUploadedJson, setFilteredUploadedJson] = useState<UploadedFileObj[]>([]);
  const [dataTableHeaders, setDataTableHeaders] = useState<string[]>([]);
  const [isFiltering, setIsFiltering] = useState<boolean>(false);

  useEffect(() => {
    const fetchUserListings = async () => {
      const purchasesRef = collection(db, 'conditions');
      const q = query(purchasesRef, where('userId', '==', user.uid));

      const querySnap = await getDocs(q);

      let listings: any = [];

      querySnap.forEach(doc => {
        return listings.push({
          id: doc.id,
          data: doc.data(),
        });
      });
      setConditionGroup(listings.find((listing: ConditionDatabaseProps) => listing.id === conditionGroupId));
    };

    fetchUserListings();
  }, [user.uid, conditionGroupId]);

  useEffect(() => {
    if (!uploadedJson) {
      return;
    }
    setDataTableHeaders(dataTableHeaders.filter((value, index, array) => array.indexOf(value) === index));
    // eslint-disable-next-line
  }, [uploadedJson]);

  useEffect(() => {
    if (!uploadedJson) {
      return;
    }
    setFilteredUploadedJson(uploadedJson);
  }, [uploadedJson]);

  const onJsonUpload = async (e: FileUploadSelectEvent) => {
    try {
      const file = e.files[0] as UploadFileType;
      const response = await fetch(file.objectURL);
      const data: UploadedFileObj[] = await response.json();
      setUploadedJson(data);

      data.forEach(object => {
        Object.keys(object).forEach(keys => {
          setDataTableHeaders(prevState => [...prevState, keys]);
        });
      });
    } catch (error) {
      toast.error('Uploading file failed');
    }
  };

  const onFilter = () => {
    setIsFiltering(true);
    let finalFilterArray: UploadedFileObj[] = filteredUploadedJson;
    conditionGroup?.data.items.forEach(item => {
      const filterArray: UploadedFileObj[] = [];
      if (dataTableHeaders.find(header => header === item.propertyName)) {
        finalFilterArray?.forEach(element => {
          if (filterConditionSwitch(element, item)) {
            filterArray.push(element);
          }
        });
      }
      finalFilterArray = filterArray;
    });
    setFilteredUploadedJson(finalFilterArray);
  };

  return (
    <>
      <NavBar />
      <main className='pb-4'>
        <div className='p-4'>
          <header className='flex justify-between items-center'>
            <div>
              <p className='text-[2rem] font-extrabold'>Script Execution</p>
              <p className='font-semibold'>{`Id: ${conditionGroupId}`}</p>
            </div>
            <div>
              {!uploadedJson ? (
                <>
                  <Tooltip target='.file-upload-button' className='text-sm shadow-none'>
                    <pre>{'Format:\n[\n\t{\n\t\t"key": "value"\n\t},\n\t{\n\t\t"key": "value"\n\t},\n]'}</pre>
                  </Tooltip>
                  <FileUpload
                    className='w-auto file-upload-button'
                    data-pr-position='bottom'
                    mode='basic'
                    name='demo[]'
                    url='/upload'
                    accept='.json'
                    maxFileSize={1000000}
                    onSelect={e => onJsonUpload(e)}
                    chooseLabel='Upload File (JSON)'
                  />
                </>
              ) : (
                <Button
                  className='border-2 rounded-lg w-full p-2 my-2 bg-red-500 hover:bg-red-700 text-white'
                  label='JSON Uploaded'
                  onClick={() => {
                    setUploadedJson(null);
                    setDataTableHeaders([]);
                  }}
                >
                  <FaTrash className='text-white ml-2' />
                </Button>
              )}
            </div>
          </header>
          <div className='h-auto flex flex-wrap gap-1 justify-between'>
            <div className='h-fit w-[39%] max-h-[150px]'>
              <div className='rounded-xl shadow-md bg-white p-4 mt-4 mb-6'>
                <h2 className='font-semibold rounded-lg mb-2'>Conditions</h2>
                <div className='text-center'>
                  <div className='overflow-y-auto h-full'>
                    {conditionGroup ? (
                      conditionGroup.data.items.map((condition: Condition) => (
                        <ConditionCard key={condition.id} data={condition} isDeleteAllowed={false} />
                      ))
                    ) : (
                      <p>No conditions found</p>
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className='h-full w-[59%] bg-white p-4 mb-4 my-4 rounded-xl shadow-md overflow-hidden'>
              <div className='flex w-full'>
                <div className='w-1/2'>
                  <h2 className='font-semibold w-full'>Object</h2>
                </div>
                <div className='w-1/2 text-end'>
                  {!isFiltering ? (
                    <Button
                      className='font-semibold w-auto border-2 rounded-lg p-2 bg-cyan-500 hover:bg-cyan-700 text-white'
                      onClick={onFilter}
                    >
                      Filter
                    </Button>
                  ) : (
                    <Button
                      className='font-semibold w-auto border-2 rounded-lg p-2 bg-red-500 hover:bg-red-700 text-white'
                      onClick={() => {
                        setIsFiltering(false);
                        setFilteredUploadedJson(uploadedJson!);
                      }}
                    >
                      UnFilter
                    </Button>
                  )}
                </div>
              </div>
              <div className='overflow-y-auto h-full mt-4'>
                {uploadedJson || dataTableHeaders.length ? (
                  <DataTable value={filteredUploadedJson} stripedRows paginator rows={5}>
                    {dataTableHeaders.map((header: string, index: number) => (
                      <Column key={index} field={header} header={header}></Column>
                    ))}
                  </DataTable>
                ) : (
                  <p>Please add an Object</p>
                )}
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default ScriptExecution;
