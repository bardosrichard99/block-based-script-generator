import { FaEquals, FaGreaterThan, FaGreaterThanEqual, FaLessThan, FaLessThanEqual, FaNotEqual } from 'react-icons/fa';

export const operatorIcon = (operator: string) => {
  switch (operator) {
    case 'Equal':
      return <FaEquals />;
    case 'Not Equal':
      return <FaNotEqual />;
    case 'Greater than':
      return <FaGreaterThan />;
    case 'Less than':
      return <FaLessThan />;
    case 'Greater than or equal':
      return <FaGreaterThanEqual />;
    case 'Less than or equal':
      return <FaLessThanEqual />;
  }
};
