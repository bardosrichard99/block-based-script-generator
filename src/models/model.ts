export interface ConditionListContextType {
  conditionListItems: Condition[];
  addToConditionList: (item: Condition) => void;
  removeFromConditionList: (itemId: number) => void;
  emptyConditionList: () => void;
}

export interface Condition {
  id: number;
  propertyName: string | number;
  operator: string;
  value: string | number;
}

export interface SignInFormDataProperties {
  email: string;
  password: string;
}

export interface SignUpFormDataProperties {
  name: string;
  email: string;
  password: string;
}

export interface FormDataCopyProperties {
  name: string;
  email: string;
  password?: string;
  timestamp?: any;
}

export interface UserType {
  email: string | null;
  uid: string | null;
  name: string | null;
}

export interface ConditionDatabaseProps {
  id: string;
  data: {
    items: Condition[];
    userId: string;
    timestamp: {
      nanoseconds: number;
      seconds: number;
    };
  };
}

export interface UploadFileType extends File {
  objectURL: string;
}
export interface UploadedFileObj {
  [key: string]: any;
}
