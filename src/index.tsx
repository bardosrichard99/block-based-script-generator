import { PrimeReactProvider } from 'primereact/api';
import Tailwind from 'primereact/passthrough/tailwind';
import 'primereact/resources/themes/lara-light-cyan/theme.css';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import App from './App';
import Profile from './Profile';
import ScriptExecution from './ScriptExecution';
import SignIn from './Signin';
import SignUp from './Signup';
import { AuthContextProvider } from './context/AuthContext';
import { ConditionListContextProvider } from './context/ConditionListContext';
import './global.css';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <PrimeReactProvider value={{ unstyled: false, pt: Tailwind }}>
      <AuthContextProvider>
        <ConditionListContextProvider>
          <Router>
            <Routes>
              <Route path='/' element={<App />}></Route>
              <Route path='/signin' element={<SignIn />}></Route>
              <Route path='/signup' element={<SignUp />}></Route>
              <Route path='/profile' element={<Profile />}></Route>
              <Route path='/execution/:conditionGroupId' element={<ScriptExecution />}></Route>
            </Routes>
          </Router>
          <ToastContainer />
        </ConditionListContextProvider>
      </AuthContextProvider>
    </PrimeReactProvider>
  </React.StrictMode>
);
