import { render } from '@testing-library/react';
import { screen } from '@testing-library/dom';
import { BrowserRouter } from 'react-router-dom';
import SignIn from './Signin';
import userEvent from '@testing-library/user-event';
import Profile from './Profile';

describe('App', () => {
  it('Login', async () => {
    render(<BrowserRouter><SignIn /></BrowserRouter>);
    const emailInput = screen.getByTestId('email-input');
    const passwordInput = screen.getByTestId('password-input');

    await userEvent.type(emailInput, 'test@test.com');
    await userEvent.type(passwordInput, 'test123');
    await userEvent.click(screen.getByTestId("submit-button"));

    render(<BrowserRouter><Profile /></BrowserRouter>);
    screen.debug();

  });
});
