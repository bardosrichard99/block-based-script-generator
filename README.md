# Block-Based Script Generator

Deployed version: [Vercel Link](https://block-based-script-generator.vercel.app).

## Repository content

- I created a `React` project with `TypeScript` and `Tailwind CSS`
- For the UI, I used `PrimeReact`'s component library to build up modern and flexible design
- The authentication and the database is provided by `Firebase`
- I used `React Icons` to display symbols
- To notify the user, I imported `React toastify`
- For testing, I used `React Testing Library` with `Jest` and `Vitest`

## About the Application

### `Script Generation`

- The Home page contains a "Condition creator" and a "list of added Conditions" section
- In the **`Condition creator`** section (left), the user can:
  - add a `property name`
  - switch between `variable types`
  - choose a `condition type` (variable type choice sets the condition type options)
  - add a `condition value` (if "Boolean" condition type is chosen, the input field becomes a dropdown with the 2 possible values)
- After filling the form, user can add the condition to the Condition list
- Under the Creator section, user can `add a JSON` file which loads the keys of the object to the property name field, changing the input field to a dropdown (with the format below)
  ```json
  {
      "Key1": "value1",
      "Key2": 1,
  }
  ```
  >
- The section on the right contains the **`list of the added conditions`**
- After adding a condition, "delete all" and "Generate script" buttons show up
- On the condition cards, user can see the condition details and an id (generated from the timestamp of the creation) for easier identification
- Every condition card has a delete button, which deletes the condition from the list
  >
- If the user clicks the "Generate script" button a **`Generate script modal`** shows up where user can: 
    - switch between `JavaScript` and `SQL` scripts. 
    - `copy` the script to the clipboard by clicking the button on the top right corner of the script box 
    - `save` the script to his/her profile (login needs) 
        > **Note:** Saving the script clears the current listing
    - `download` the script in .txt file
> **Note:** Home page is always reachable from the navigation bar

### `Login`

- From the Home page, we can navigate to the SignIn page by clicking the user icon in the navigation bar
- On the **`SignIn page`**, user can fill the `Email and Password` fields and click the sign in button
- Alternatively, user can choose `Google authentication` by clicking the button with Google logo
- If user doesn't have an account, he/she can click the "Sign up instead" button which navigates to the SignUp page
- On the **`SignUp page`**, user can fill the `Name, Email and Password` fields and click the sign up button or choose Google authentication again
  > **Note:** The login status is displayed with a tick on the user icon in the navigation bar

### `Profile page`

- After login, profile page is reachable by clicking the user icon in the navigation bar
- On the **`Profile page`**, user can:
  - `log out` by clicking the button in the top right corner
  - see the `account details`
  - navigate back to the home page
  - browsing between the saved `Condition groups`
- On the Condition Group cards, user can see the id of the condition group, the time of the saving and the containing conditions
- Every Condition Group card has an `Execution button` which navigates to the Execution page

### `Script Execution`

- After user clicks the Execution button on Profile page, Execution page is loading in with:
  - the `id` of the Condition group
  - the `Object uploading button`
  - the `condition list` of the Condition group
  - section for the filtering result
- User should upload the object array what he/she wants to filter (with the format below)
  ```json
  [
    {
      "Key1": "value1",
      "Key2": 1
    },
    {
      "Key1": "value2",
      "Key2": 2
    }
  ]
  ```
- After user uploads the JSON, it loads into a DataTable (with pagination implemented)
- User can `filter` and `unfilter` the DataTable with the Filter button

## Things I would do if I would have more time

- Adding more property name types
- Adding more Condition types (eg: "Contains")
- Adding more Login options (eg: Facebook, Github)
- Creating a "404" page for undefined routes
- Set redirecting to home page when we navigate to profile page while not logged in
- Adding script generator option to already saved Conditions groups
- Adding script upload option
- Implementing function to update account data
- All conditions are linked, no option for setting conditions between the conditions\
(eg: (Condition1 AND Condition2) OR (Condition3 AND Condition4))
- Implement responsivity for the whole application (Mobile and Tablet view)
- Complete Unit testing for the whole application
- Dark and light mode
- There are libraries which can help generating .js and .sql files, not just .txt
